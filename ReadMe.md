
This project was made by me as part of the idea of a game with infinite level generation

As a result of working on the mechanics, I came to a prototype of a roguelike game.
Basic concepts of the game

* The character must fight with opponents
* The character must collect coins to upgrade his attack power and HP.
* Opponents grow in strength depending on how far the character has gone from the start
* The world is generated in front of the user endlessly
* The character can move around the platforms on the level
* The character can climb onto the platforms from below and jump off them
* The user can improve the character's performance by using coins found on the level
* the game must have a user interface
    -Character health <br/>
    -Enemy health <br/>
    -Pause menu <br/>
    -Menu when the character died <br/>
    -Restart the level <br/>
* The character and opponents die
* It is possible to save the results of the game
* The game should have background music
* In the game, the player's actions must have a soundtrack

# Download 
You can download the builds of the project in order to familiarize yourself with it
Android: https://drive.google.com/drive/folders/1-BdJiCEf0nZioq3M0FGouFJ6xAm-_INA?usp=sharing <br/>
PC: https://drive.google.com/drive/folders/1n_zLUiwHKBdHqHhYuRc4jTmzq1La_WDk?usp=sharing

# Illustration
![MenuInfinityRuner](/uploads/4c47e4d2d6092b4403a9690f7b41c117/MenuInfinityRuner.png)
![GameInfinityRuner](/uploads/58109190451b3960766ac9614e0c7f20/GameInfinityRuner.png)
