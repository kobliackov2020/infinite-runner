using System;
using UnityEngine;
using UnityEngine.UI;

public class EnemyHeath : MonoBehaviour, IHealth
{
    [SerializeField] private Slider _healthUI;
    [SerializeField] private float _totalHeath = 100f;
    [SerializeField] private float _buffToHealth = 5f;
    [SerializeField] private float _distanceToBuff = 200;
    [SerializeField] private Animator _animator;

    private float _health;

    public Action Died;
    // Start is called before the first frame update
    void Start()
    {
        _health = _totalHeath + ((int)(transform.position.x / _distanceToBuff) * _buffToHealth);
        SetHeathUI();
    }
    public void ReduceHeath(float damage)
    {
        _health -= damage;
        SetHeathUI();
        _animator.SetTrigger("takeDamage");
        if (_health <= 0)
        {
            Die();
        }
    }
    public void Kill()
    {
        _health -= _totalHeath;
        Die();
    }

    private void SetHeathUI()
    {
        _healthUI.value = _health / _totalHeath;
    }
    private void Die()
    {
        Died?.Invoke();
        _animator.SetTrigger("Die");
    }

    public void IncreaseHealth(float recoveryPover)
    {
        _health += recoveryPover;
        SetHeathUI();
    }
}
