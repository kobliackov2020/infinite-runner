using System;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    [SerializeField] private Transform _enemyModelTransform;
    [SerializeField] private Animator animator;
    [SerializeField] private Transform _startPatrol;
    [SerializeField] private Transform _endPatrol;
    [SerializeField] private float _patrolSpeed = 1f;
    [SerializeField] private float _chaiseingSpeed = 3f;
    [SerializeField] private float _timeToWait = 5f;
    [SerializeField] private float _timeToChaise = 5f;
    [SerializeField] private int[] _reactThisLayer;

    private Vector2 _leftBoundaryPosition;
    private Vector2 _rightBoundaryPosition;
    private Vector2 nextPoint;
    private bool _isFacingRight = true;
    private bool _isWait = false;
    private bool _collidedWthisPlayer = false;
    private float _waitTime;
    private float _chaisingTime;
    private float _walkSpeed;
    private Transform _player;

    public bool IsFacingRight => _isFacingRight;

    // Start is called before the first frame update
    void Start()
    {
        if (_startPatrol != null && _endPatrol != null)
        {
            _leftBoundaryPosition = _startPatrol.position;
            _rightBoundaryPosition = _endPatrol.position;
        }
        else
        {
            _leftBoundaryPosition = transform.position;
            _rightBoundaryPosition = transform.position;
        }
        _waitTime = _timeToWait;
        _walkSpeed = _patrolSpeed;
    }

    // Update is called once per frame
    void Update()
    {
        if (_player != null)
        {
            StartChaisingTimer();
        }
        if (_isWait && _player == null)
        {
            animator?.SetFloat("SpeedX", 0);

            StarWaitTimer();
        }

        if (ShouldWait())
        {
            _isWait = true;
        }
    }

    private void FixedUpdate()
    {
        nextPoint = Vector2.right * _walkSpeed * Time.fixedDeltaTime;
        if (_player != null && _collidedWthisPlayer)
        {
            return;
        }

        if (_player != null)
        {
            ChasePlayer();
        }

        if (!_isWait && _player == null)
        {
            Patrol();
        }
    }
    public void SetPatrolPoints(Transform start, Transform end)
    {
        _startPatrol = start;
        _leftBoundaryPosition = start.position;
        _endPatrol = end;
        _rightBoundaryPosition = end.position;
    }

    private void Patrol()
    {
        if (!_isFacingRight)
        {
            nextPoint.x *= -1;
        }
        MoveToNextPoint();
    }

    private void ChasePlayer()
    {
        float distance = GetDistanceToPlayer();
        if (distance < 0)
        {
            nextPoint.x *= -1;
        }
        if (distance > 0.2f && !_isFacingRight)//���� ����� ������ � ����� ������� �����
        {
            Flip();
        }
        else if (distance < 0.2f && IsFacingRight)
        {
            Flip();
        }
        MoveToNextPoint();

    }

    private float GetDistanceToPlayer()
    {
        return _player.position.x - transform.position.x;
    }

    private void MoveToNextPoint()
    {
        //_rb.MovePosition((Vector2)transform.position + nextPoint);
        if (_walkSpeed == _patrolSpeed)
        {
            animator?.SetFloat("SpeedX", 0.2f);
        }
        else
        {
            animator?.SetFloat("SpeedX", 1);
        }

        transform.position = new Vector3(transform.position.x + nextPoint.x, transform.position.y, transform.position.z);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(_leftBoundaryPosition, _rightBoundaryPosition);
    }

    public void StartChaisingPlayer(Transform player)
    {
        _player = player;
        _chaisingTime = _timeToChaise;
        _walkSpeed = _chaiseingSpeed;
    }

    private void Flip()
    {
        _isFacingRight = !_isFacingRight;
        Vector3 playerScale = _enemyModelTransform.localScale;
        playerScale.x *= -1;
        _enemyModelTransform.localScale = playerScale;
    }

    private void StarWaitTimer()
    {
        _waitTime -= Time.deltaTime;
        if (_waitTime < 0)
        {
            _isWait = false;
            _waitTime = _timeToWait;
            Flip();
        }
    }
    public void StartChaisingTimer()
    {
        _chaisingTime -= Time.deltaTime;
        if (_chaisingTime < 0)
        {
            _player = null;
            _chaisingTime = _timeToChaise;
            _walkSpeed = _patrolSpeed;
        }
    }

    private bool ShouldWait()
    {
        bool _isOutOfRightBoundary = _isFacingRight && transform.position.x >= _rightBoundaryPosition.x;
        bool _isOutOfLeftBoundary = !_isFacingRight && transform.position.x <= _leftBoundaryPosition.x;
        return _isOutOfLeftBoundary || _isOutOfRightBoundary;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            _collidedWthisPlayer = true;
        }
        for (int i = 0; i < _reactThisLayer.Length; i++)
        {
            if (collision.gameObject.layer == _reactThisLayer[i])
            {
                if (collision.transform.position.x < transform.position.x && _isFacingRight)
                {
                    Flip();
                }
                else if (collision.transform.position.x > transform.position.x && !_isFacingRight)
                {
                    Flip();
                }
            }
        }
        
    }
    private void OnCollisionExit2D(Collision2D collision)
    {
        PlayerController player = collision.gameObject.GetComponent<PlayerController>();
        if (player != null)
        {
            _collidedWthisPlayer = false;
        }
    }

}
