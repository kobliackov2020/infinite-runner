using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackRange : MonoBehaviour
{
    private bool _isPlayerInRange;
    public bool IsPlayerInRange => _isPlayerInRange;
   
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var player = collision.gameObject.GetComponent<PlayerHealth>();
        if (player != null)
        {
            _isPlayerInRange = true;
        }
        else
        {
            _isPlayerInRange = false;
        }
    }
    private void OnTriggerExit2D(Collider2D collision)
    {
        var player = collision.gameObject.GetComponent<PlayerHealth>();
        if (player != null)
        {
            _isPlayerInRange = false;
        }
    }
}
