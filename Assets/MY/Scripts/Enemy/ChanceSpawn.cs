using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChanceSpawn : MonoBehaviour
{
    [SerializeField] private AnimationCurve _spawnChance;
    public AnimationCurve SpawnChance => _spawnChance;
    private float chance;
    private float value;
    private void Start()
    {
        chance = _spawnChance.Evaluate(transform.position.x);
        value = Random.Range(0f, 1f);
        if (value > chance)
        {
            gameObject.SetActive(false);
        }
    }
}
