using UnityEngine;

public class EnemyDrop : MonoBehaviour
{
    private DropsItems _dropsItems;
    private EnemyHeath _enemyHeath;
    private void Awake()
    {
        _enemyHeath = gameObject.GetComponent<EnemyHeath>();
        _dropsItems = GetComponent<DropsItems>();
    }
    private void OnEnable()
    {
        _enemyHeath.Died += DropItsms;
    }
    private void OnDisable()
    {
        _enemyHeath.Died -= DropItsms;
    }

    private void DropItsms()
    {
        _dropsItems.DropItsms();
    }
}

