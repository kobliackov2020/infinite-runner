using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RougeAttack : MonoBehaviour
{
    [SerializeField] private float _baseDamage = 15f;
    [SerializeField] private float _buffToDamage = 5f;
    [SerializeField] private float _distanceToBuff = 200f;
    [SerializeField] private float _timeToDamage = 1f;
    [SerializeField] private Animator animator;
    private float _timeDamage;
    private bool _isAttackReady = true;
    private float _damge;
    private void Start()
    {
        _timeDamage = _timeToDamage;
        _damge = _baseDamage + ((int)(transform.position.x / _distanceToBuff) * _buffToDamage);
    }
    private void Update()
    {
        if (!_isAttackReady)
        {
            _timeDamage -= Time.deltaTime;
            if (_timeDamage <= 0)
            {
                _isAttackReady = true;
                _timeDamage = _timeToDamage;
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        PlayerHealth playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
        var distance = Vector3.Distance(collision.transform.position, transform.position);

        if (playerHealth != null && _isAttackReady)
        {
            _isAttackReady = false;
            animator.SetTrigger("Attack");
            animator.SetFloat("DistanceToPlayer", distance);
            playerHealth.ReduceHeath(_damge);
        }
    }
}
