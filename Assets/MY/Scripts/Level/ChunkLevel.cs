using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChunkLevel : MonoBehaviour
{
    [SerializeField] private Transform _begin;
    [SerializeField] private Transform _end;
    public Transform Begin => _begin;
    public Transform End => _end;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
