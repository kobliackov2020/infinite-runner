using System;
using UnityEngine;

public class SceneController : MonoBehaviour
{
    [SerializeField] private GameObject _canvaseGameOver;
    [SerializeField] private GameObject _player;
    [SerializeField] private float _timeToDie = 2f;
    [SerializeField] private TMPro.TextMeshProUGUI _coinsUI;
    [SerializeField] private TMPro.TextMeshProUGUI _distanceUI;
    private PlayerStats _playerStats;
    private PlayerHealth _playerHealth;
    private float _dieTime;
    private bool _isDie;
    // Start is called before the first frame update

    private void Awake()
    {
        PlayerController playerController = _player.GetComponent<PlayerController>();
        _playerHealth = _player.GetComponent<PlayerHealth>();
        _playerStats = playerController.PlayerStats;
        Time.timeScale = 1f;
        UpdateCoins();  
    }

    void Start()
    {
        _dieTime = _timeToDie;
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_isDie)
        {
            _dieTime -= Time.deltaTime;
            if (_dieTime <= 0)
            {
                _canvaseGameOver.SetActive(true);
            }
        }
        if (_player.transform.position.x > 0)
        {
            string distance = _player.transform.position.x.ToString("F0") + " m";
            _distanceUI.text = distance;
        }
        else
        {
            _distanceUI.text = $"0 m";
        }
    }
    private void OnEnable()
    {
        _playerHealth.Death += PlayerDie;
        _playerStats.CoinsCountChange += UpdateCoins;
    }

   

    private void OnDisable()
    {

        _playerHealth.Death -= PlayerDie;
        _playerStats.CoinsCountChange -= UpdateCoins;
    }

    public void PlayerDie()
    {
        _isDie = true;
    }
    private void UpdateCoins()
    {
        _coinsUI.text = _playerStats.CoinsCount.ToString();
    }
}
