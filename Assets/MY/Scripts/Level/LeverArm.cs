using UnityEngine;

public class LeverArm : MonoBehaviour
{
    [SerializeField] private Finish _finish;
    [SerializeField] private Animator animator;
   
    public void ActivateLeverArm()
    {
        _finish.Activate();
        animator.SetTrigger("activate");
    }
}
