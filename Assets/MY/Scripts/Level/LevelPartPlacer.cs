using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelPartPlacer : MonoBehaviour
{
    [SerializeField] private Transform _player;
    [SerializeField] private ChunkLevel[] _chunkLevelPrefabs;
    [SerializeField] private ChunkLevel _firstChunk;
    [SerializeField] private float _spavnOffset = 20;
    [SerializeField] private int _chunkCount = 3;

    private List<ChunkLevel> spawnedChunks = new List<ChunkLevel>();
    // Start is called before the first frame update
    void Start()
    {
        spawnedChunks.Add(_firstChunk);
    }

    // Update is called once per frame
    void Update()
    {
        if (_player.transform.position.x > spawnedChunks[spawnedChunks.Count - 1].End.position.x - _spavnOffset)
        {
            SpawnChunk();
        }
    }
    private void SpawnChunk()
    {
        ChunkLevel newChunk = Instantiate(_chunkLevelPrefabs[Random.Range(0, _chunkLevelPrefabs.Length)]);

        newChunk.transform.position = spawnedChunks[spawnedChunks.Count - 1].End.position - newChunk.Begin.localPosition;

        spawnedChunks.Add(newChunk);

        if (spawnedChunks.Count > _chunkCount)
        {
            Destroy(spawnedChunks[0].gameObject);
            spawnedChunks.RemoveAt(0);
        }
    }
}
