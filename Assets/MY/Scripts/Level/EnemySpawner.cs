using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private List<EnemyController> _enemys;
    [SerializeField] private Transform _start;
    [SerializeField] private Transform _end;
    [SerializeField] private int _enemyCount;
    [SerializeField] private float _spawnDelay;
    private int _currentCount;
    // Start is called before the first frame update
    private void Start()
    {
        _currentCount = 0;
        StartCoroutine(nameof(Spawn));
    }
    IEnumerator Spawn()
    {
        for (_currentCount = 0; _currentCount < _enemyCount; _currentCount++)
        {
            int i = Random.Range(0, _enemys.Count);
            var clone = Instantiate(_enemys[i], transform);
            clone.transform.position = transform.position;
            if (_start != null && _end != null)
            {
                clone.SetPatrolPoints(_start, _end);
            }
            yield return new WaitForSeconds(_spawnDelay);
        }
    }
}
