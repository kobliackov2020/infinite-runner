using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyRandomChilds : MonoBehaviour
{
    [SerializeField] private int _countToLeave = -1;
    // Start is called before the first frame update
    void Start()
    {
        if (_countToLeave == -1)
        {
            _countToLeave = Random.Range(0, transform.childCount);
        }
        while (transform.childCount > _countToLeave)
        {
            Transform childToDestroy = transform.GetChild(Random.Range(0, transform.childCount));
            DestroyImmediate(childToDestroy.gameObject);
        }
    }
}
