using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeadZone : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        var health = collision.GetComponent<IHealth>();
        if (health != null)
        {
            health.Kill();
        }
    }
   
}
