using UnityEditor;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Finish : MonoBehaviour
{
    [SerializeField] private GameObject _toolTipCanvas;
    //[SerializeField] private SceneAsset nextScene;

    private bool _isActivated;

    public void Activate()
    {
        _isActivated = true;
        _toolTipCanvas.SetActive(false);
    }
    public void FinisLevel()
    {
        if (_isActivated)
        {
            gameObject.SetActive(false);
            //SceneManager.LoadScene(nextScene.name);
            return;
        }
        _toolTipCanvas.SetActive(true);
    }
}
