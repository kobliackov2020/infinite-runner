using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Increase Button", menuName = "ScriptableObjects/IncreaseButton", order = 1)]
public class IncreaseButtonScriptables : ScriptableObject, IPersisted
{
    [SerializeField] private int _costUpgrade = 5;
    [SerializeField] private int _costNextLevel = 5;
    [SerializeField] private int _defaulCost = 5;
    public int CostUpgrade => _costUpgrade;
    public void IncreaceCost()
    {
        _costUpgrade += _costNextLevel;        
    }

    public void ResetData()
    {
        _costUpgrade = _defaulCost;
    }

    public void SetCostUpgrade(int cost)
    {
        _costUpgrade = cost;
    }

    public void SaveData()
    {
        //EditorUtility.SetDirty(this);
        //AssetDatabase.SaveAssets();
    }
    
}
