using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IncreaceShopButton : MonoBehaviour
{
    [SerializeField] private IncreaseButtonScriptables _increaseButtonScriptable;
    [SerializeField] private TMPro.TextMeshProUGUI _costUIText;
    public int CostUpgrade => _increaseButtonScriptable.CostUpgrade;
    // Start is called before the first frame update
    void Start()
    {
        UpdateButtonText();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void UpdateButtonText()
    {
        _costUIText.text = CostUpgrade.ToString();
    }

    public void IncreaceCost()
    {
        _increaseButtonScriptable.IncreaceCost();
        UpdateButtonText();
    }
}
