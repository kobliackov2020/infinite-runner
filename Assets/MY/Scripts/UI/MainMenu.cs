using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    [SerializeField] private GameObject _shopCanvas;
    [SerializeField] private DataManager _dataManager;
    public void NewGameHandler()
    {
        SceneManager.LoadScene(1);
        _dataManager.ResetData();
    }
    public void ContinueHandler()
    {

        SceneManager.LoadScene(1);//�������� �� �������
        // �������� �� ����� 
        //SceneManager.LoadScene("GameScene");
    }
    public void ShopHandler()
    {
        gameObject.SetActive(false);
        _shopCanvas.SetActive(true);
    }
    public void ExitHandler()
    {
        Application.Quit();
    }
}
