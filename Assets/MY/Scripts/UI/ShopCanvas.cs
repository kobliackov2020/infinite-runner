using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopCanvas : MonoBehaviour
{
    [SerializeField] private GameObject _mainCanvas;
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private IncreaceShopButton AttackButton;
    [SerializeField] private IncreaceShopButton HealthButton;
    [SerializeField] private TMPro.TextMeshProUGUI AttackUIText;
    [SerializeField] private TMPro.TextMeshProUGUI HealthUIText;
    [SerializeField] private TMPro.TextMeshProUGUI CoinsUIText;
    private void OnEnable()
    {
        UpdateAttackUI();
        UpdateHealthUI();
        UpdateCounsUI();
    }
    public void BackHandler()
    {
        gameObject.SetActive(false);
        _mainCanvas.SetActive(true);
    }
    public void IncreaseAttack()
    {
        if (AttackButton.CostUpgrade <= _playerStats.CoinsCount)
        {
            _playerStats.IncreaceBuffAttack(5);
            _playerStats.ReduceCoins(AttackButton.CostUpgrade);
            UpdateAttackUI();
            AttackButton.IncreaceCost();
            UpdateCounsUI();
        }
    }
    public void IncreaseHealth()
    {
        if (HealthButton.CostUpgrade <= _playerStats.CoinsCount)
        {
            _playerStats.IncreaceBuffHealth(5);
            _playerStats.ReduceCoins(HealthButton.CostUpgrade);
            UpdateHealthUI();
            HealthButton.IncreaceCost();
            UpdateCounsUI();
        }        
    }
    private void UpdateAttackUI()
    {
        AttackUIText.text = $"Buff attack: {_playerStats.BuffAttack}";
    }
    
    private void UpdateHealthUI()
    {
        HealthUIText.text = $"Buff health: {_playerStats.BuffHeath}";
    }
    private void UpdateCounsUI()
    {
        CoinsUIText.text = _playerStats.CoinsCount.ToString();
    }
}
