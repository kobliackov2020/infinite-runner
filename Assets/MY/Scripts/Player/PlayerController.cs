using System;
using System.Collections;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private Animator _animator;
    [SerializeField] private AudioSource _jumpSourse;
    [SerializeField] private Transform _playerModelTransform;
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private float _speed;
    [SerializeField] private float _jumpForce;
    [SerializeField] private GameObject _fx;
    [SerializeField] private float _jerkDistance = 5f;
    [SerializeField] private int _jerkCount = 1;
    [Header("Intput")]
    [SerializeField] private bool _isMobileInput;
    [SerializeField] private FixedJoystick _fixedJoystick;
    public PlayerStats PlayerStats => _playerStats;

    private Rigidbody2D _rb;
    private float _horizontal = 0f;
    const float _speedMultiplayer = 50f;
    private float delayGrounding = 0.21f;
    private int _currentJerk;
    private bool _isFacingRight = true;
    private bool _isGround = true;
    private bool _isJump = false;
    private bool _isJerk;
    private bool _isCrouch;
    //private Finish _finish;
    //private LeverArm _leverArm;

    public bool IsGround => _isGround;
    public bool IsFacingRight => _isFacingRight;
    // Start is called before the first frame update
    void Start()
    {
        Time.timeScale = 1f;
        _currentJerk = _jerkCount;
        _rb = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        if (_isMobileInput)
        {
            _horizontal = _fixedJoystick.Horizontal;
            if (_fixedJoystick.Vertical < -0.5f && _isGround)
            {
                _animator.SetBool("Crouch", true);
                _isCrouch = true;
            }
            else
            {
                _animator.SetBool("Crouch", false);
                _isCrouch = false;
            }
        }
        else
        {
            _horizontal = Input.GetAxis("Horizontal");
            if (Input.GetKeyDown(KeyCode.Space))
            {
                InputJump();
            }
            if (Input.GetKey(KeyCode.S) && _isGround)
            {
                _animator.SetBool("Crouch", true);
                _isCrouch = true;
            }
            else
            {
                _animator.SetBool("Crouch", false);
                _isCrouch = false;
            }
            if (Input.GetKeyDown(KeyCode.LeftShift))
            {
                InputJerk();
            }
        }
        _animator.SetFloat("SpeedX", Mathf.Abs(_horizontal));
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        _rb.velocity = new Vector2(_horizontal * _speed * _speedMultiplayer * Time.fixedDeltaTime, _rb.velocity.y);

        if (_isJump && !_isCrouch)
        {
            Jump();
        }
        if (_isJerk)
        {
            Jerk();
        }
        if (_isCrouch && _isJump)
        {
            FallOffPlatform();
        }
        if (_horizontal > 0f && !_isFacingRight)
        {
            Flip();
        }
        else if (_horizontal < 0f && _isFacingRight)
        {
            Flip();
        }
    }

    private void FallOffPlatform()
    {
        Physics2D.IgnoreLayerCollision(3, 8, true);
        Invoke(nameof(IgnoreLAyerOff), 0.8f);
        _animator.SetBool("Crouch", false);
        if (_rb.velocity.y <= 0)
        {
            _animator.SetTrigger("Jump");
        }

        _isGround = false;
        _isCrouch = false;
        _isJump = false;
    }

    private void IgnoreLAyerOff()
    {        
        Physics2D.IgnoreLayerCollision(3, 8, false);
    }

    private void Jerk()
    {
        _currentJerk -= 1;
        Vector2 dir;
        if (IsFacingRight)
        {
            dir = Vector2.right;
        }
        else
        {
            dir = Vector2.left;
        }

        _rb.AddForce(new Vector2(0, _jumpForce / 3));
        _rb.MovePosition((Vector2)transform.position + dir * _jerkDistance);

        GameObject fx = Instantiate(_fx);
        fx.transform.position = new Vector3(transform.localPosition.x - (0.5f * dir.x), transform.localPosition.y - 1, fx.transform.position.z);
        fx.transform.localScale = new Vector3(fx.transform.localScale.x * dir.x, 1, 1);
        _isJerk = false;
        _isGround = false;
    }

    private void Jump()
    {
        _rb.AddForce(new Vector2(0, _jumpForce));
        _isGround = false;
        _isJump = false;
    }

    private void Flip()
    {
        _isFacingRight = !_isFacingRight;
        Vector3 playerScale = _playerModelTransform.localScale;
        playerScale.x *= -1;
        _playerModelTransform.localScale = playerScale;
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        ChekGrouding(collision);
    }

    private void ChekGrouding(Collision2D collision)
    {
        if ((delayGrounding -= Time.fixedDeltaTime) < 0)
        {
            if (collision.gameObject.CompareTag("Ground"))
            {
                if (_isGround == false)
                {
                    _animator.SetTrigger("IsGround");
                }
                _currentJerk = _jerkCount;
                _isGround = true;
                delayGrounding = 0.25f;
            }
        }
        
    }
    public void InputJump()
    {
        if (_isGround)
        {
            _isJump = true;
            _jumpSourse.Play();
            _animator.SetTrigger("Jump");
        }
    }
    
    public void InputJerk()
    {
        if (_currentJerk > 0)
        {
            _isJerk = true;
            _jumpSourse.Play();
            _animator.SetTrigger("Strike");
        }
    }
}
