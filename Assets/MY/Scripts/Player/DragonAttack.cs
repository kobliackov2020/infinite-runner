using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DragonAttack : MonoBehaviour, IAttacking
{
    [SerializeField] private AudioSource _attackSourse;
    [SerializeField] private Animator animator;
    [SerializeField] private GameObject _fireball;
    [SerializeField] private GameObject _blast;
    [SerializeField] private Image _fireballImage;
    [SerializeField] private Image _blastImage;
    [SerializeField] private float _proggresToBlast = 5f;
    [SerializeField] private float _proggresToFireball = 1f;
    [Header("Intput")]
    [SerializeField] private bool _isMobileInput;

    private PlayerController _player;
    private float _currentProgresToBlast = 0f;
    private float _currentProgresToFireball = 0f;

    private bool _isAttack;
    private bool _isBlastReady;
    private bool _isFireballReady;

    public bool IsAttack => _isAttack;
    private void OnEnable()
    {
        _player = GetComponent<PlayerController>();
    }
    public void FinishAttack()
    {
        _isAttack = false;
    }

    private void Update()
    {
        if (!_isMobileInput)
        {
            if (Input.GetMouseButtonDown(0))
            {
                Attack();
            }
            if (Input.GetKeyDown(KeyCode.F) )
            {
                SuperAttack();
            }
            if (Input.GetMouseButtonDown(1))
            {
                Kick();
            }
        }
        if (!_isBlastReady)
        {
            StartBlastTimer();
        }
        if (!_isFireballReady)
        {
            StartFireballTimer();
        }
    }

    public void Kick()
    {
        if (_player.IsGround)
        {
            _isAttack = true;
            _attackSourse.Play();
            animator.SetTrigger("Kick");
        }
    }
    public void SuperAttack()
    {
        if (_isBlastReady)
        {
            Fire(_blast);
            _isBlastReady = false;
        }
        
    }
    public void Attack()
    {
        if (_isFireballReady)
        {
            Fire(_fireball);
            _isFireballReady = false;
        }
    }
    private void Fire(GameObject ammunition)
    {
        animator.SetTrigger("Attack");
        Vector2 directional = _player.IsFacingRight ? Vector2.right : Vector2.left;
        GameObject inst = Instantiate(ammunition);
        inst.transform.position = transform.position;
        inst.GetComponent<Ammunition>().Instantiate(_player.PlayerStats.BuffAttack, directional);
    }

    private void StartBlastTimer()
    {
        _currentProgresToBlast -= Time.deltaTime;
        if (_blastImage != null)
        {
            _blastImage.fillAmount = _currentProgresToBlast / _proggresToBlast;
        }
        if (_currentProgresToBlast <= 0)
        {
            _isBlastReady = true;
            _currentProgresToBlast = _proggresToBlast;
            if (_blastImage != null)
            {
                _blastImage.fillAmount = 0;
            }
        }
    }
    private void StartFireballTimer()
    {
        _currentProgresToFireball -= Time.deltaTime;
        if (_fireballImage != null)
        {
            _fireballImage.fillAmount = _currentProgresToFireball / _proggresToFireball;
        }
        if (_currentProgresToFireball <= 0)
        {
            _isFireballReady = true;
            _currentProgresToFireball = _proggresToFireball;
            if (_fireballImage != null)
            {
                _fireballImage.fillAmount = 0;
            }
        }
    }
}
