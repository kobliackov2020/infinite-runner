using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KickAttack : MonoBehaviour
{
    [SerializeField] private AudioSource _hitSourse;
    [SerializeField] private float _damage = 20f;
    private DragonAttack _attack;
    private PlayerController _player;
    private void Start()
    {
        _player = transform.root.GetComponent<PlayerController>();
        _damage += _player.PlayerStats.BuffAttack;
        _attack = transform.root.GetComponent<DragonAttack>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        EnemyHeath enemyHealth = other.GetComponent<EnemyHeath>();
        Debug.Log(enemyHealth);

        if (enemyHealth != null && _attack.IsAttack)
        {
            enemyHealth.ReduceHeath(_damage);
            _hitSourse.Play();
        }
    }
}
