using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CreateAssetMenu(fileName = "Player Data", menuName = "ScriptableObjects/PlayerData", order = 1)]
public class PlayerStats : ScriptableObject, IPersisted
{
    [Header("Cerrent Value")]
    [SerializeField] private float _buffAttack = 0f;
    [SerializeField] private float _buffHealth = 0f;
    [SerializeField] private int _coinsCount = 0;

    [Header("Default Value")]
    [SerializeField] private float _defaultBuffValue = 0f;
    [SerializeField] private int _defaultCoinsCount = 0;

    public Action CoinsCountChange;

    public float BuffAttack
    {
        get => _buffAttack;
        set
        {
            if (value >= 0)
            {
                _buffAttack = value;
            }
        }
    }
    public float BuffHeath
    {
        get => _buffHealth;
        set
        {
            if (value >= 0)
            {
                _buffHealth = value;
            }
        }
    }
    public int CoinsCount
    {
        get => _coinsCount;
        set
        {
            if (value >= 0)
            {
                _coinsCount = value;
            }
        }
    }

    public void IncreaceBuffAttack(float increaceValue)
    {
        BuffAttack += increaceValue;
    }
    public void ReduceBuffAttack(float reduceValue)
    {
        BuffAttack -= reduceValue;
    }
    public void IncreaceBuffHealth(float increaceValue)
    {
        BuffHeath += increaceValue;
    }
    public void ReduceBufHealth(float reduceValue)
    {
        BuffHeath -= reduceValue;
    }
    public void IncreaceCoins(int increaceValue)
    {
        CoinsCount += increaceValue;
        CoinsCountChange?.Invoke();
    }
    public void ReduceCoins(int reduceValue)
    {
        if (CoinsCount - reduceValue < 0)
        {
            throw new ArgumentException("Invalid value, the number of coins cannot be less than zero");
        }
        CoinsCount -= reduceValue;
        CoinsCountChange?.Invoke();
    }

    public void SaveData()
    {
        //EditorUtility.SetDirty(this);
        //AssetDatabase.SaveAssets();
    }
    public void ResetData()
    {
        _buffAttack = _defaultBuffValue;
        _buffHealth = _defaultBuffValue;
        _coinsCount = _defaultCoinsCount;
    }
}
