using UnityEngine;

public class Weapon : MonoBehaviour
{
    [SerializeField] private AudioSource _hitSourse;
    [SerializeField] private float _damage = 20f;
    private Attack _attack;
    private PlayerController _player;
    private void Start()
    {
        _player = transform.root.GetComponent<PlayerController>();
        _damage += _player.PlayerStats.BuffAttack;
        _attack = transform.root.GetComponent<Attack>();
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        EnemyHeath enemyHealth = other.GetComponent<EnemyHeath>();
        if (enemyHealth != null && _attack.IsAttack)
        {
            enemyHealth.ReduceHeath(_damage);
            _hitSourse.Play();
        }
    }

}
