using UnityEngine;

public class Attack : MonoBehaviour, IAttacking
{
    [SerializeField] private AudioSource _attackSourse;
    [SerializeField] Animator animator;
    private bool _isAttack;
    public bool IsAttack => _isAttack;

    public void FinishAttack()
    {
        _isAttack = false;
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            _isAttack = true;
            _attackSourse.Play();
            animator.SetTrigger("Attack");
        }
        if (Input.GetMouseButtonDown(1))
        {
            _isAttack = true;
            _attackSourse.Play();
            animator.SetTrigger("Kick");
        }
    }
}
public interface IAttacking
{
    public bool IsAttack { get; }
    public void FinishAttack();
}