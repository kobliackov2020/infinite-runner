using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ammunition : MonoBehaviour
{
    [SerializeField] private AudioSource _source;
    [SerializeField] private AudioClip _audioHit;
    [SerializeField] private AudioClip _audioFly;
    [SerializeField] private float _damage = 20f;
    [SerializeField] private float _speed = 10;
    public void Instantiate(float _buffAttack, Vector2 directional)
    {
        _damage += _buffAttack;
        transform.localScale = new Vector3 (transform.localScale.x * directional.x, transform.localScale.y,transform.localScale.z);
        GetComponent<Rigidbody2D>().AddForce(directional * _speed, ForceMode2D.Impulse);
        _source.Play();
    }
    private void OnEnable()
    {
        _source.clip = _audioFly;
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        EnemyHeath enemyHealth = collision.gameObject.GetComponent<EnemyHeath>();
        if (enemyHealth != null)
        {
            enemyHealth.ReduceHeath(_damage);
        }
        DestroyAmmunition();
    }
    private void DestroyAmmunition()
    {
        _source.clip = _audioHit;
        _source.Play();
        StartCoroutine(DestroyAfterAudio());
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
    IEnumerator DestroyAfterAudio()
    {
        yield return new WaitForSeconds(_source.clip.length);
        Destroy(gameObject);
    }
}
