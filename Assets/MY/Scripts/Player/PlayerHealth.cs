using System;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour, IHealth
{
    [SerializeField] private AudioSource _hurtSourse;
    [SerializeField] private Animator _animator;
    [SerializeField] private Slider _healthUI;
    [SerializeField] private float _totalHeath = 100f;
    private float _health;
    private PlayerController _player;
    public Action Death;
    public float Health 
    {
        get 
        {
            return _health; 
        } 
        private set 
        { 
            if (value < 0)
            {
                _health = 0;
                return;
            }
            if (value > _totalHeath)
            {
                _health = _totalHeath;
                return;
            }
            _health = value; 
        } 
    }
    private void Start()
    {
        _player = GetComponent<PlayerController>();
        _totalHeath += _player.PlayerStats.BuffHeath;
        _health = _totalHeath;
        SetHealthUI();
    }

    public void IncreaseHealth(float recoveryPover)
    {
        Health += recoveryPover;
        SetHealthUI();
    }

    public void ReduceHeath(float damage)
    {
        Health -= damage;
        SetHealthUI();
        _hurtSourse.Play();
        _animator.SetTrigger("takeDamage");
        if (_health <= 0)
        {
            Die();
        }
    }
 
    public void Kill()
    {
        Health -= _totalHeath;
        Die();
    }

    private void SetHealthUI()
    {
        _healthUI.value = _health / _totalHeath;
    }
    private void Die()
    {
        _animator.SetTrigger("Die");
        Death?.Invoke();    
    }
}
public interface IHealth
{
    public void ReduceHeath(float damage);
    public void IncreaseHealth(float recoveryPover);
    public void Kill();
}