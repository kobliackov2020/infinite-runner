using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpWhenActivated : MonoBehaviour
{
    [SerializeField] private float _force = 250;
    // Start is called before the first frame update
    void OnEnable()
    {
        var rb = GetComponent<Rigidbody2D>();
        rb.AddForce(new Vector2(transform.position.x + Random.Range(-50f, 50f),_force));
    }
}
