using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    [SerializeField] private int _cost;
    private AudioSource _source;
    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            //_source.Play();
            AudioSource.PlayClipAtPoint(_source.clip, this.gameObject.transform.position, 1f);
            playerController.PlayerStats.IncreaceCoins(_cost);
            DestroyCoin();
        }
    }
    private void DestroyCoin()
    {
        _source.Play();
        StartCoroutine(DestroyAfterAudio());
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
    IEnumerator DestroyAfterAudio()
    {
        yield return new WaitForSeconds(_source.clip.length);
        Destroy(gameObject);
    }
}
