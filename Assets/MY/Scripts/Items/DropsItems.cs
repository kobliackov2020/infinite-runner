using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

public class DropsItems : MonoBehaviour
{
    [SerializeField] DropGroup[] didscardedGroups;

    public void DropItsms()
    {
        for (int i = 0; i < didscardedGroups.Length; i++)
        {
            if (didscardedGroups[i].Chance / 100f > Random.value)
            {
                GameObject item = didscardedGroups[i].Drop();
                if (item != null)
                {
                    Instantiate(item, new Vector2(transform.position.x + Random.Range(-1f,1f), transform.position.y + Random.Range(0, 1f)), new Quaternion());
                }

            }
        }
    }
}


[Serializable]
public class DroppedItem
{
    public GameObject item;
    [Range(0, 100)]
    public float Chance;

    public float GetChance()
    {
        return Chance / 100;
    }
}

/// <summary>
/// only one element drops out of the group
/// </summary>
[Serializable]
public class DropGroup
{
    public DroppedItem[] DidscardedGroups;
    public float Chance;

    public GameObject Drop()
    {
        if (DidscardedGroups.Length > 1)
        {
            List<float> chances = new List<float>();
            for (int i = 0; i < DidscardedGroups.Length; i++)
            {
                chances.Add(DidscardedGroups[i].GetChance());
            }
            float value = Random.Range(0, chances.Sum());
            float sum = 0;
            for (int i = 0; i < chances.Count; i++)
            {
                sum += chances[i];
                if (value < sum)
                {
                    return DidscardedGroups[i].item;
                }
            }
            return DidscardedGroups[DidscardedGroups.Length - 1].item;
        }
        else
        {
            if (DidscardedGroups[0].Chance > Random.value)
            {
                return DidscardedGroups[0].item;
            }
            return null;
        }

    }
}
