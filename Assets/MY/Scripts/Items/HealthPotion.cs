using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPotion : MonoBehaviour
{
    [SerializeField] private float _recoveryPover;
    private AudioSource _source;
    private void Start()
    {
        _source = GetComponent<AudioSource>();
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        PlayerHealth playerHealth = collision.gameObject.GetComponent<PlayerHealth>();
        if (playerHealth != null)
        {
            _source.Play();
            playerHealth.IncreaseHealth(_recoveryPover);
            DestroyPotion();
        }
    }
    private void DestroyPotion()
    {
        _source.Play();
        StartCoroutine(DestroyAfterAudio());
        GetComponent<Collider2D>().enabled = false;
        GetComponent<SpriteRenderer>().enabled = false;
    }
    IEnumerator DestroyAfterAudio()
    {
        yield return new WaitForSeconds(_source.clip.length);
        Destroy(gameObject);
    }
}
