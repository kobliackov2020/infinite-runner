using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestDrop : MonoBehaviour
{
    private DropsItems _dropsItems;
    private Animator _animator;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        PlayerController playerController = collision.gameObject.GetComponent<PlayerController>();
        if (playerController != null)
        {
            _animator.SetTrigger("Open");
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        _dropsItems = GetComponent<DropsItems>();
    }

    public void Drop()
    {
        _dropsItems.DropItsms();
    }
}
