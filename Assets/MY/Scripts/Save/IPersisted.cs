﻿public interface IPersisted
{
    public void SaveData();
    public void ResetData();
}
