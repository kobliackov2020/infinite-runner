using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour
{
    [SerializeField] private PlayerStats _playerStats;
    [SerializeField] private IncreaseButtonScriptables _attack;
    [SerializeField] private IncreaseButtonScriptables _health;
    public static bool _isLoadDataToStart = true;
    private void Awake()
    {
        if (_isLoadDataToStart)
        {
            LoadData();
            _isLoadDataToStart = false;
        }
    }
    private void OnApplicationQuit()
    {
        SaveData();
    }

    public void LoadData()
    {
        SaveData saveData = SaveSystem.LoadGameData();
        if (saveData != null)
        {
            _playerStats.BuffAttack = saveData.buffAttack;
            _playerStats.BuffHeath = saveData.buffHealt;
            _playerStats.CoinsCount = saveData.coinsCount;
            _attack.SetCostUpgrade(saveData.costBuffAttak);
            _health.SetCostUpgrade(saveData.costBuffHealt);
        }
    }
    public void SaveData()
    {
        SaveSystem.SaveGameData(_playerStats, _attack, _health);
    }
    public void ResetData()
    {
        _playerStats.ResetData();
        _attack.ResetData();
        _health.ResetData();
    }

}
