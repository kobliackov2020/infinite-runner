using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public static class SaveSystem 
{
    public static void SaveGameData(PlayerStats playerStats, IncreaseButtonScriptables attackButton, IncreaseButtonScriptables healthButton)
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = Application.persistentDataPath + "/GameData.save";
        FileStream stream = new FileStream(path, FileMode.Create);
        SaveData saveData = new SaveData(playerStats, attackButton, healthButton);
        formatter.Serialize(stream, saveData);
        stream.Close();
    }
    public static SaveData LoadGameData()
    {
        string path = Application.persistentDataPath + "/GameData.save";
        if (File.Exists(path))
        {
            BinaryFormatter formatter = new BinaryFormatter();
            FileStream stream = new FileStream(path, FileMode.Open);
            SaveData saveData = formatter.Deserialize(stream) as SaveData;
            stream.Close();
            return saveData;
        }
        else
        {
            Debug.LogError("Save file not found in " + path);
            return null;
        }

    }
}
