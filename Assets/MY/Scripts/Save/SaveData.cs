using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData 
{
    public float buffAttack;
    public float buffHealt;
    public int coinsCount;
    public int costBuffAttak;
    public int costBuffHealt;

    public SaveData(PlayerStats playerStats, IncreaseButtonScriptables attackButton, IncreaseButtonScriptables healthButton)
    {
        buffAttack = playerStats.BuffAttack;
        buffHealt = playerStats.BuffHeath;
        coinsCount = playerStats.CoinsCount;
        costBuffAttak = attackButton.CostUpgrade;
        costBuffHealt = healthButton.CostUpgrade;
    }
}
