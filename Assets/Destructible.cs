﻿using System;
using UnityEngine;

internal class Destructible : MonoBehaviour
{
    [SerializeField] private GameObject _destructible;
    public GameObject Destructibled => _destructible;
}